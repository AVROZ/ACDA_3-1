package fr.iutfbleau.projet;

import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.event.MouseInputListener;

public class GalerieListener implements MouseInputListener {

    int compteur = 1;
    int winX;
    int winY;
    JFrame window;
    JLabel image;

    public GalerieListener(JFrame window, JLabel image) {
        this.window = window;
        this.image = image;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getX() >= (window.getWidth()/2)) {
            window.remove(image);
            if (compteur == 1) {
                image = new JLabel(new ImageIcon("res/image2.jpg"));
                compteur = 2;
            }
            else {
                image = new JLabel(new ImageIcon("res/image1.jpg"));
                compteur = 1;
            }
            window.add(image);
        }
        else {
            window.remove(image);
            if (compteur == 1) {
                image = new JLabel(new ImageIcon("res/image2.jpg"));
                compteur = 2;
            }
            else {
                image = new JLabel(new ImageIcon("res/image1.jpg"));
                compteur = 1;
            }
            window.add(image);
        }

        window.validate();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseMoved(MouseEvent e) {
        // TODO Auto-generated method stub

    }
    
}