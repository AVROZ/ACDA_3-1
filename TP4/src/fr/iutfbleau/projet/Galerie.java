package fr.iutfbleau.projet;

import javax.swing.*;

public class Galerie {
    public static void main(String[] args) {
        JFrame fenetre = new JFrame();
        fenetre.setSize(500, 500);
        fenetre.setLocation(100, 100);
        fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel image1 = new JLabel(new ImageIcon("res/image1.jpg"));
        image1.setHorizontalAlignment(JLabel.CENTER);
        
        fenetre.add(image1);

        GalerieListener listener = new GalerieListener(fenetre, image1);
        fenetre.addMouseListener(listener);
        
        fenetre.setVisible(true);
    }
    
}