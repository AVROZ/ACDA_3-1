import javax.swing.*;
import java.awt.*;

public class GalerieCard {
    public static void main(String[] args) {
        JFrame fenetre = new JFrame();
        fenetre.setSize(500, 500);
        fenetre.setLocation(100, 100);
        fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        CardLayout layMan = new CardLayout();
        fenetre.setLayout(layMan);
        
        JLabel image1 = new JLabel(new ImageIcon("image1.jpg"));
        JLabel image2 = new JLabel(new ImageIcon("image2.jpg"));
        image1.setHorizontalAlignment(JLabel.CENTER);
        image2.setHorizontalAlignment(JLabel.CENTER);
        
        fenetre.add(image1);

        GalerieListener listener = new GalerieListener(fenetre, image1);
        fenetre.addMouseListener(listener);
        
        fenetre.setVisible(true);
    }
    
}