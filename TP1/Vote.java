import java.sql.*;
import org.mariadb.jdbc.*;

/**
 * Vote
 */
public class Vote {

    public static void main(String[] args) {
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.exit(1);
        }
        try {
            Connection cnx = DriverManager.getConnection("jdbc:mariadb://dwarves.iut-fbleau.fr/queudot",
            "queudot",
            "queudot");
        } catch (SQLException e) {
            System.out.println("Erreur de connexion");
        }
        
        try {
            PreparedStatement pst = cnx.prepareStatement("INSERT INTO Vote VALUES (?,?,?,?)");
            pst.setInt(1, 27);
            pst.executeUpdate();
            pst.setString(2, "test");
            pst.executeUpdate();
            pst.setString(3, "test");
            pst.executeUpdate();
            pst.setInt(4, 84);
            pst.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Problème d'insertion de tuple");
        }

        PreparedStatement pst = cnx.prepareStatement("SELECT Points FROM Vote WHERE Vote = ?");
        pst.setString(1, args[1]);
        ResultSet rs = pst.executeQuery();
    }
}