import java.awt.event.*;
import javax.swing.*;

public class BoutonsListener implements WindowListener {

    @Override
    public void windowActivated(WindowEvent we) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void windowClosed(WindowEvent we) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void windowClosing(WindowEvent we) {
        String ObjButtons[] = {"Oui", "Non"};
        int PromptResult = JOptionPane.showOptionDialog(null, 
            "Voulez-vous vraiment quitter ?", "Online Examination System", 
            JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, 
            ObjButtons, ObjButtons[1]);
        if(PromptResult==0) {
            System.exit(0);
        }
        
    }

    @Override
    public void windowDeactivated(WindowEvent we) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void windowDeiconified(WindowEvent we) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void windowIconified(WindowEvent we) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void windowOpened(WindowEvent we) {
        // TODO Auto-generated method stub
        
    }
    
}
