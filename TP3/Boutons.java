import javax.swing.*;
import java.awt.*;

public class Boutons {

    public static int quitter() {
        int result = JOptionPane.showConfirmDialog((Component) null, "Voulez-vous vraiment quitter ?", "alert", JOptionPane.YES_NO_CANCEL_OPTION);
        return result;
    }


    public static void main(String[] args) {

        JFrame fenetre = new JFrame();
        fenetre.setSize(500, 500);
        fenetre.setLocation(100, 100);
        fenetre.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        fenetre.addWindowListener(new BoutonsListener());

        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());

        JButton b1 = new JButton("1");
        JButton b2 = new JButton("2");
        JButton b3 = new JButton("3");
        JButton b4 = new JButton("4");
        JButton b5 = new JButton("5");
        b5.setPreferredSize(new Dimension(100, 100));

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0; // la plage de cellules commence à la première colonne
        gbc.gridy = 0; // la plage de cellules commence à la deuxième ligne
        gbc.gridwidth = 2; // la plage de cellules englobe deux colonnes
        gbc.gridheight = 1; // la plage de cellules englobe une seule ligne
        gbc.fill = GridBagConstraints.BOTH; // n'occupe pas tout l'espace de la plage
        gbc.anchor = GridBagConstraints.WEST; // se place au centre de la plage
        gbc.weightx = 1.0; // souhaite plus de largeur si possible
        gbc.weighty = 1.0; // n'a pas besoin de hauteur supplémentaire
        panel.add(b1, gbc);

        gbc.gridx = 2;
        gbc.gridy = 0; // la plage de cellules commence à la deuxième ligne
        gbc.gridwidth = 1; // la plage de cellules englobe deux colonnes
        gbc.gridheight = 2; // la plage de cellules englobe une seule ligne
        gbc.fill = GridBagConstraints.BOTH; // n'occupe pas tout l'espace de la plage
        gbc.anchor = GridBagConstraints.WEST; // se place au centre de la plage
        gbc.weightx = 1.0; // souhaite plus de largeur si possible
        gbc.weighty = 1.0; // n'a pas besoin de hauteur supplémentaire
        panel.add(b2, gbc);

        gbc.gridx = 1;
        gbc.gridy = 2; // la plage de cellules commence à la deuxième ligne
        gbc.gridwidth = 2; // la plage de cellules englobe deux colonnes
        gbc.gridheight = 1; // la plage de cellules englobe une seule ligne
        gbc.fill = GridBagConstraints.BOTH; // n'occupe pas tout l'espace de la plage
        gbc.anchor = GridBagConstraints.WEST; // se place au centre de la plage
        gbc.weightx = 1.0; // souhaite plus de largeur si possible
        gbc.weighty = 1.0; // n'a pas besoin de hauteur supplémentaire
        panel.add(b3, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1; // la plage de cellules commence à la deuxième ligne
        gbc.gridwidth = 1; // la plage de cellules englobe deux colonnes
        gbc.gridheight = 2; // la plage de cellules englobe une seule ligne
        gbc.fill = GridBagConstraints.BOTH; // n'occupe pas tout l'espace de la plage
        gbc.anchor = GridBagConstraints.WEST; // se place au centre de la plage
        gbc.weightx = 1.0; // souhaite plus de largeur si possible
        gbc.weighty = 1.0; // n'a pas besoin de hauteur supplémentaire
        panel.add(b4, gbc);

        gbc.gridx = 1;
        gbc.gridy = 1; // la plage de cellules commence à la deuxième ligne
        gbc.gridwidth = 1; // la plage de cellules englobe deux colonnes
        gbc.gridheight = 1; // la plage de cellules englobe une seule ligne
        gbc.fill = GridBagConstraints.BOTH; // n'occupe pas tout l'espace de la plage
        gbc.anchor = GridBagConstraints.WEST; // se place au centre de la plage
        gbc.weightx = 0.0; // souhaite plus de largeur si possible
        gbc.weighty = 0.0; // n'a pas besoin de hauteur supplémentaire
        panel.add(b5, gbc);

        fenetre.add(panel);
        fenetre.setVisible(true);
    }
}